var xhr = new XMLHttpRequest();
function submitRequest() {
    var typeSelector = document.getElementById('type');
    var type = typeSelector.options[typeSelector.selectedIndex].value;
    var amount = document.getElementById('amount').value;
    var description = document.getElementById('description').value;
    var requestJSON = {
        "type": type,
        "amount": amount,
        "description": description,
        "userID": 0
    };
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            var data = xhr.responseText;
            alert(data);
            window.location.href="emphome.html";   
        }
    };
    xhr.open('POST', '/Project1Maven/Request');
    xhr.send(JSON.stringify(requestJSON));
	return false;
}

function verifySession() {
    var xhrUser = new XMLHttpRequest();
	xhrUser.onreadystatechange = function () {
		if (xhrUser.readyState == XMLHttpRequest.DONE) {
			let data = xhrUser.responseText;
			if (!data) {
				//not logged in, must redirect
				//falsy empty string...
				window.location.href = "login.html";
			} else {
				let upr = JSON.parse(data);
				if(upr.role.role != "EMPLOYEE") {
					window.location.href = "login.html";
				} else {
					document.getElementById("unheader").innerHTML = upr.username;
				}
			}
		}
	};
	xhrUser.open('POST', '/Project1Maven/CheckSession');
	xhrUser.send();
}