function updateAll() {
	verifySession();
	updateRows();
}

function verifySession() {
	let xhrUser = new XMLHttpRequest();
	xhrUser.onreadystatechange = function() {
		if (xhrUser.readyState == XMLHttpRequest.DONE) {
			let data = xhrUser.responseText;
			if (!data) {
				//not logged in, must redirect
				//falsy empty string...
				window.location.href = "login.html";
			} else {
				let upr = JSON.parse(data);
				if(upr.role.role != "EMPLOYEE") {
					window.location.href = "login.html";
				} else {
					document.getElementById("unheader").innerHTML = upr.username;
				}
			}
		}
	};
	xhrUser.open('POST', '/Project1Maven/CheckSession');
	xhrUser.send();
}


function updateRows() {
	var reqrows = document.getElementById("requestrows");
	while(reqrows.firstChild){
		reqrows.removeChild(reqrows.firstChild);
	}

	var xhrRows = new XMLHttpRequest();
	xhrRows.onreadystatechange = function () {
		if (xhrRows.readyState == XMLHttpRequest.DONE) {
			var jget = xhrRows.responseText;
			var objArray = JSON.parse(jget);
			for (var o of objArray) {
				reqrows.append(getRow(o.id, o.type.type, o.amount, o.description, o.status.status));
			}
		}
	};


	xhrRows.open('POST', '/Project1Maven/EmpHomeGetRows');
	xhrRows.send();

}

function getRow(id, type, amount, description, status) {
	var newrow = document.createElement("DIV");
	newrow.setAttribute("class", "row align-items-center request-row");

	var newid = document.createElement("DIV");
	newid.setAttribute("class", "col-md-2");
	newid.innerHTML = "ID: " + id;

	var newtype = document.createElement("DIV");
	newtype.setAttribute("class", "col-md-2");
	newtype.innerHTML = "Type: " + type;

	var newamount = document.createElement("DIV");
	newamount.setAttribute("class", "col-md-2");
	newamount.innerHTML = "Amount: $" + amount.toFixed(2);

	var newdescription = document.createElement("DIV");
	newdescription.setAttribute("class", "col-md-4");
	newdescription.innerHTML = "Description: " + description;

	var newstatus = document.createElement("DIV");
	newstatus.setAttribute("class", "col-md-2");
	newstatus.innerHTML = "Status: " + status;

	newrow.appendChild(newid);
	newrow.appendChild(newtype);
	newrow.appendChild(newamount);
	newrow.appendChild(newdescription);
	newrow.appendChild(newstatus);


	return newrow;
}