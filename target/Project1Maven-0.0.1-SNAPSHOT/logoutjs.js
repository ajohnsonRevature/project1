function logout() {
    var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (xhr.readyState == XMLHttpRequest.DONE) {
            window.location.href = "login.html";
		}
	};
	xhr.open('POST', '/Project1Maven/Logout');
	xhr.send();
}