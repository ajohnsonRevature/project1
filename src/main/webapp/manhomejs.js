function updateAll() {
	verifySession();
	updateRows();
	updateEmps();
}

function updateEmps() {
	let xhrEmps = new XMLHttpRequest();
	let empsels = document.getElementById("reimbempselect");
	for(let o of empsels.childNodes) {
		if(o.id != "all") {
			empsels.removeChild(o);
		}
	}
	xhrEmps.onreadystatechange = function() {
		if (xhrEmps.readyState == XMLHttpRequest.DONE) {
			let data = xhrEmps.responseText;
			let emps = JSON.parse(data);
			for(let e of emps) {
				empsels.appendChild(getEmpOption(e));
			}
		}
	};
	xhrEmps.open('POST', '/Project1Maven/ManHomeGetEmps');
	xhrEmps.send();
}

function getEmpOption(e) {
	let opt = document.createElement("OPTION");
	opt.innerHTML = "ID: " + e.id + " " + e.pi.firstName + " " + e.pi.lastName;
	opt.setAttribute("id", e.id);
	return opt;
}

function verifySession() {
	let xhrUser = new XMLHttpRequest();
	xhrUser.onreadystatechange = function() {
		if (xhrUser.readyState == XMLHttpRequest.DONE) {
			let data = xhrUser.responseText;
			if (!data) {
				//not logged in, must redirect
				//falsy empty string...
				window.location.href = "login.html";
			} else {
				let upr = JSON.parse(data);
				if(upr.role.role != "MANAGER") {
					window.location.href = "login.html";
				} else {
					document.getElementById("unheader").innerHTML = upr.username;
				}
			}
		}
	};
	xhrUser.open('POST', '/Project1Maven/CheckSession');
	xhrUser.send();
}

function updateRows() {
	let reqrows = document.getElementById("requestrows");
	while(reqrows.firstChild){
		reqrows.removeChild(reqrows.firstChild);
	}

	let empselector = document.getElementById("reimbempselect");  
	let emp = empselector.options[empselector.selectedIndex].id;
	
	let xhrRows = new XMLHttpRequest();
	xhrRows.onreadystatechange = function() {
		if (xhrRows.readyState == XMLHttpRequest.DONE) {
			let jget = xhrRows.responseText;
			let objArray = JSON.parse(jget);
			for (let o of objArray) {
				reqrows.append(getRow(o.id, o.type.type, o.userID, o.amount, o.description, o.status.status));
			}
		}
	};
	xhrRows.open('POST', '/Project1Maven/ManHomeGetRows');
	xhrRows.setRequestHeader("Content-type", "text/plain");
	xhrRows.send(emp);

}

function approvedeny(rid, mode) {
	let xhr = new XMLHttpRequest();
	let adr = {"rid":rid, "mode":mode};
	xhr.onreadystatechange = function() {
		if (xhr.readyState == XMLHttpRequest.DONE) {
			let resp = xhr.responseText;
			if(!resp) {
				alert("Something went wrong! :(");
			} else {
				document.getElementById("appcol " + rid).remove();
				document.getElementById("dencol " + rid).remove();
				let newstatus = "Status: ";
				newstatus += (mode == "approve") ? "APPROVED" : "DENIED";
				document.getElementById("statid " + rid).innerHTML = newstatus;
			}
		}
	};
	xhr.open('POST', '/Project1Maven/ApproveDeny');
	xhr.send(JSON.stringify(adr));
}

function getRow(id, type, userID, amount, description, status) {
	let newrow = document.createElement("DIV");
	newrow.setAttribute("class", "row align-items-center request-row");
	let newid = document.createElement("DIV");
	newid.setAttribute("class", "col-md-1");
	newid.innerHTML = "ID: " + id;

	let newtype = document.createElement("DIV");
	newtype.setAttribute("class", "col-md-1");
	newtype.innerHTML = "Type: " + type;

	let newuserid = document.createElement("DIV");
	newuserid.setAttribute("class", "col-md-2");
	newuserid.innerHTML = "Requested by User ID: " + userID;

	let newamount = document.createElement("DIV");
	newamount.setAttribute("class", "col-md-2");
	newamount.innerHTML = "Amount: $" + amount.toFixed(2);

	let newdescription = document.createElement("DIV");
	newdescription.setAttribute("class", "col-md-3");
	newdescription.innerHTML = "Description: " + description;

	let newstatus = document.createElement("DIV");
	newstatus.setAttribute("class", "col-md-1");
	newstatus.setAttribute("id", "statid " + id);
	newstatus.innerHTML = "Status: " + status;

	newrow.appendChild(newid);
	newrow.appendChild(newtype);
	newrow.appendChild(newuserid);
	newrow.appendChild(newamount);
	newrow.appendChild(newdescription);
	newrow.appendChild(newstatus);
	if (status == "PENDING") {
		let approvebutcol = document.createElement("DIV");
		approvebutcol.setAttribute("class", "col-md-1");
		approvebutcol.setAttribute("id", "appcol " + id);
		let approvebut = document.createElement("BUTTON");
		approvebut.setAttribute("type", "button");
		approvebut.setAttribute("class", "btn btn-success");
		approvebut.setAttribute("onclick", "approvedeny(" + id + ", \"approve\")");
		approvebut.innerHTML = "Approve";
		approvebutcol.appendChild(approvebut);

		let denybutcol = document.createElement("DIV");
		denybutcol.setAttribute("class", "col-md-1");
		denybutcol.setAttribute("id", "dencol " + id);
		let denybut = document.createElement("BUTTON");
		denybut.setAttribute("type", "button");
		denybut.setAttribute("class", "btn btn-danger");
		denybut.setAttribute("onclick", "approvedeny(" + id + ", \"deny\")");
		denybut.innerHTML = "Deny";
		denybutcol.appendChild(denybut);

		newrow.appendChild(approvebutcol);
		newrow.appendChild(denybutcol);
	}
	return newrow;
}