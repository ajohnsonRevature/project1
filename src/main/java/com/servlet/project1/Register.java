package com.servlet.project1;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import exceptions.NonexistingUserException;
import users.User;
import users.UserOracleDAO;
import users.UserPersInfo;

/**
 * Servlet implementation class Register
 */
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Register() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ObjectMapper ob = new ObjectMapper();
		response.setContentType("text/plain");
		try {
			User u = ob.readValue(request.getReader(), User.class);
			if (u == null) {
				response.getWriter().append("Unexpected error registering user!");
			}
			try {
				UserOracleDAO.getInstance().getUserByUsername(u.getUPR().getUsername());
				response.getWriter().append("This username is taken!");
				response.setHeader("redirect", "#");
			} catch (NonexistingUserException e) {
				// this is what we want actually!
				if (UserOracleDAO.getInstance().insertUser(u)) {
					response.getWriter().append("Registration successful!");
					response.setHeader("redirect", "./login.html");
				} else {
					//failed to add user for some reason
					response.getWriter().append("Database error - registration failed. :(");
					response.setHeader("redirect", "#");
				}
			}
		} catch (InvalidFormatException e) {
			response.getWriter().append("Unable to parse request! D:");
			response.setHeader("redirect", "#");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
