package com.servlet.project1;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import users.*;

/**
 * Servlet implementation class UpdateInfo
 */
public class UpdateInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ObjectMapper ob = new ObjectMapper();
		response.setContentType("text/plain");
		try {
			UserPersInfo upi = ob.readValue(request.getReader(), UserPersInfo.class);
			User u = (User) request.getSession().getAttribute("loggedInUser");
			if(u == null) {
				response.getWriter().append("");
			}
			u.setUPI(upi);
			if (UserOracleDAO.getInstance().updateUser(u)) {
				response.getWriter().append("Personal info updated successfully. :)");
			} else {
				response.getWriter().append("Personal info failed to update. :(");
			}
		} catch (InvalidFormatException e) {
			response.getWriter().append("Unable to parse request! D:");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
