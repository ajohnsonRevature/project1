package reimbursements;

import java.io.Serializable;

public class ReimbursementType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReimbursementType(String type) {
		super();
		this.type = type.toUpperCase();
	}

	private String type;
	
	@Override
	public String toString() {
		return type.toUpperCase();
	}
}
