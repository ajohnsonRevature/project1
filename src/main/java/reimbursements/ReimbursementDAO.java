package reimbursements;

import java.util.List;

import users.User;

public interface ReimbursementDAO {
	public List<Reimbursement> getAllReimbursements();
	public List<Reimbursement> getAllReimbursementsByUser(User u);
	public boolean insertReimbursement(Reimbursement reimbToInsert);
	public boolean updateReimbursement(Reimbursement reimbToUpdate);
	public Reimbursement getReimbursementByID(int id);
}
