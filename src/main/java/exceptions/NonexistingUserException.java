package exceptions;

public class NonexistingUserException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public String getMessage() {
		return "This user account could not be found in the database!";
	}

}
