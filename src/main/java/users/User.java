package users;

import java.io.Serializable;

import exceptions.NonexistingUserException;

public class User implements Serializable {

	
	private int id;
	private UserUPR upr;
	private UserPersInfo pi;
	
	public User() {
		
	}
	
	public User(int id, UserUPR upr, UserPersInfo pi) {
		super();
		this.id = id;
		this.setUPR(upr);
		this.pi = pi;
	}
	
	public UserPersInfo getUPI() {
		return pi;
	}
	
	public void setUPI(UserPersInfo pi) {
		this.pi = pi;
	}
	
	public static boolean validateUnPw(String username, String password) throws NonexistingUserException {
		return UserOracleDAO.getInstance().getUserByUsername(username).getUPR().getPassword().equals(password);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public UserUPR getUPR() {
		return upr;
	}

	public void setUPR(UserUPR upr) {
		this.upr = upr;
	}

	public UserPersInfo getPi() {
		return pi;
	}

	public void setPi(UserPersInfo pi) {
		this.pi = pi;
	}
}
