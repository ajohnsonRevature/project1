package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import database.DB;
import exceptions.NonexistingUserException;
import reimbursements.Reimbursement;
import reimbursements.ReimbursementOracleDAO;
import reimbursements.ReimbursementStatus;
import reimbursements.ReimbursementType;
import users.User;
import users.UserOracleDAO;
import users.UserPersInfo;
import users.UserRole;
import users.UserUPR;

public class ReimbursementDAOTests {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DB.closeConn();
	}

	@Before
	public void setUp() throws Exception {
		DB.truncateAll();
	}

	@After
	public void tearDown() throws Exception {
		DB.truncateAll();
	}

	@Test
	public void testInsertReimbursement() {
		User u = new User(45, new UserUPR("AlexE", "12345", new UserRole("EMPLOYEE")), new UserPersInfo("Alex", "Employee", "alexe@gmail.com"));
		UserOracleDAO.getInstance().insertUser(u);
		int uid = 0;
		try {
			uid = UserOracleDAO.getInstance().getUserByUsername("AlexE").getId();
		} catch (NonexistingUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Reimbursement r = new Reimbursement(0, 123.45, uid, "yummy", new ReimbursementType("FOOD"),
				new ReimbursementStatus("PENDING"));
		assertTrue(ReimbursementOracleDAO.getInstance().insertReimbursement(r));
	}

}
